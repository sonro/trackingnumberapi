<?php

include_once(dirname(__FILE__) . '/CronJson.php');

class CronJsonResponse extends CronJson
{
    /**
     * @var bool
     */
    public $executed;

    /**
     * @var bool
     */
    public $successful;

    /**
     * @var array
     */
    public $errors;

    /**
     * @var array
     */
    public $result;

    /**
     * @param array|null $data = null
     */
    public function __construct($data = null)
    {
        $this->setDataProperty($data, 'executed', false);
        $this->setDataProperty($data, 'successful', false);
        $this->setDataProperty($data, 'errors', array());
        $this->setDataProperty($data, 'result', array());
    }

    /**
     * Create From JSON.
     *
     * @param string $json
     *
     * @return CronJson
     */
    public static function createFromJson($json)
    {
        $data = json_decode($json, true);
        foreach ($data['errors'] as $key => $value) {
            $data['errors'][$key] = new CronJsonError($value);
        }

        return new self($data);
    }
}


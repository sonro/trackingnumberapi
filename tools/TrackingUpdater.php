<?php

include_once(dirname(__FILE__) . '/../../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../../init.php');
include_once(dirname(__FILE__) . '/../CronJson/CronJsonResponse.php');
include_once(dirname(__FILE__) . '/../CronJson/CronJsonError.php');

class TrackingUpdater
{
    public static function update($data)
    {
        $cronJsonResponse = new CronJsonResponse();
        $cronJsonResponse->executed = true;

        foreach ($data as $orderInput) {
            $id = $orderInput['id'];
            $trackingNumber = $orderInput['trackingNumber'];
            $order = new Order($id);

            if (!$order->current_state) {
                $cronJsonResponse->errors[] = new CronJsonError([
                    'title' => 'TrackingNumberApi: Invalid Order',
                    'code' => 11,
                    'detail' => 'Order '.$id
                                .' in shop '.Configuration::get('PS_SHOP_NAME'),
                ]);
            } else if (!Validate::isTrackingNumber($trackingNumber)) {
                $cronJsonResponse->errors[] = new CronJsonError([
                    'title' => 'TrackingNumberApi: Invalid Tracking Number',
                    'code' => 12,
                    'detail' => 'Order '.$id.'with tracking number '
                                .$trackingNumber.' in shop '
                                .Configuration::get('PS_SHOP_NAME'),
                ]);
            } else {
                self::setShippingNumber($order, $trackingNumber, $cronJsonResponse);
            }
        }

        $cronJsonResponse->successful = true;

        return $cronJsonResponse;
    }

    private static function setShippingNumber($order, $trackingNumber, $cronJsonResponse)
    {
        $order_carrier = new OrderCarrier($order->getIdOrderCarrier());

        // update shipping number
        // Keep these two following lines for backward compatibility, remove on 1.6 version
        $order->shipping_number = $trackingNumber;
        $order->update();

        // Update order_carrier
        $order_carrier->tracking_number = $trackingNumber;
        if ($order_carrier->update()) {
            // Send mail to customer
            $customer = new Customer((int)$order->id_customer);
            $carrier = new Carrier((int)$order->id_carrier, $order->id_lang);
            if (!Validate::isLoadedObject($customer)) {
                $cronJsonResponse->errors[] = new CronJsonError([
                    'title' => 'TrackingNumberApi: Prestashop Exception',
                    'code' => 21,
                    'detail' => 'Order '.$order->id.' in shop '
                                .Configuration::get('PS_SHOP_NAME')
                                .': Can\'t load customer object',
                ]);
                return;
            }
            if (!Validate::isLoadedObject($carrier)) {
                $cronJsonResponse->errors[] = new CronJsonError([
                    'title' => 'TrackingNumberApi: Prestashop Exception',
                    'code' => 22,
                    'detail' => 'Order '.$order->id.' in shop '
                                .Configuration::get('PS_SHOP_NAME')
                                .': Can\'t load Carrier object',
                ]);
                return;
            }
            $templateVars = array(
                '{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
                '{firstname}' => $customer->firstname,
                '{lastname}' => $customer->lastname,
                '{id_order}' => $order->id,
                '{shipping_number}' => $order->shipping_number,
                '{order_name}' => $order->getUniqReference()
            );

            if (!@Mail::Send(
                (int)$order->id_lang,
                'in_transit',
                Mail::l('Package in transit', (int)$order->id_lang),
                $templateVars,
                $customer->email,
                $customer->firstname.' '.$customer->lastname, 
                null, 
                null, 
                null, 
                null,
                _PS_MAIL_DIR_, 
                true, 
                (int)$order->id_shop
            )) {
                $cronJsonResponse->errors[] = new CronJsonError([
                    'title' => 'TrackingNumberApi: Mail error',
                    'code' => 23,
                    'detail' => 'Order '.$order->id.' in shop '
                                .Configuration::get('PS_SHOP_NAME')
                                .': Mail sending issue to '
                                .$customer->firstname.' '.$customer->lastname.', '
                                .$customer->email,
                ]);
            } else {
                $cronJsonResponse->result[] = $order->id;
            }
        } else {
            $cronJsonResponse->errors[] = new CronJsonError([
                'title' => 'TrackingNumberApi: Unable to update',
                'code' => 24,
                'detail' => 'Order '.$order->id.' in shop '
                .Configuration::get('PS_SHOP_NAME'),
            ]);
        }

        return;
    }
}

<?php

chdir(dirname(__FILE__));
include_once(dirname(__FILE__) . '/../../config/config.inc.php');
include_once(dirname(__FILE__) . '/../../init.php');
include_once(dirname(__FILE__) . '/CronJson/CronJsonResponse.php');
include_once(dirname(__FILE__) . '/CronJson/CronJsonError.php');
include_once(dirname(__FILE__) . '/tools/TrackingUpdater.php');

header('Content-Type: application/json');
$cronJsonResponse = new CronJsonResponse();

if (substr(_COOKIE_KEY_, 34, 8) != Tools::getValue('token')) {
    $cronJsonResponse->errors[] = new CronJsonError([
        'title' => 'TrackingNumberApi: Invalid token',
        'code' => 1,
        'detail' => 'Incorrect token supplied to TrackingNumberApi cron job',
    ]);
} else if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    $cronJsonResponse->errors[] = new CronJsonError([
        'title' => 'TrackingNumberApi: Not a POST request',
        'code' => 2,
        'detail' => 'TrackingNumberApi must be a POST request',
    ]);
} else if ($_SERVER['CONTENT_TYPE'] !== 'application/json') {
    $cronJsonResponse->errors[] = new CronJsonError([
        'title' => 'TrackingNumberApi: POST body must be json',
        'code' => 3,
        'detail' => 'TrackingNumberApi only accepts application/json',
    ]);
} else {

    $content = trim(file_get_contents("php://input"));

    //Attempt to decode the incoming RAW post data from JSON.
    $decoded = json_decode($content, true);

    //If json_decode failed, the JSON is invalid.
    if(!is_array($decoded)){
        $cronJsonResponse->errors[] = new CronJsonError([
            'title' => 'TrackingNumberApi: Invalid JSON body',
            'code' => 4,
            'detail' => 'Syntax error in JSON',
        ]);
    } else {
        $cronJsonResponse = TrackingUpdater::update($decoded);
    }
}

echo $cronJsonResponse->toJson();
